//
//  ViewController.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

class ViewController<T>: UIViewController {
    
    var viewModel: T!
    
    var hideNavigationBar: Bool {
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(hideNavigationBar, animated: animated)
    }

    func bindViewModel(_ viewModel: T) {
        self.viewModel = viewModel
    }

}

