//
//  SearchWordsViewController.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

final class SearchWordsViewController: ViewController<SearchWordsViewModel> {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private let searchView = UISearchBar()
    
    private let unitsBuilder: SearchWordsUnitsBuilder = .init()
    
    private let tableViewModel: BasicTableManager = .init()
    
    var flowWordDetails: Block<Word>?

    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
        configureTableView()
        configureObservers()
        configureActions()
    }
    
    override func bindViewModel(_ viewModel: SearchWordsViewModel) {
        super.bindViewModel(viewModel)
        
        viewModel.isLoading.bind(self) { [weak self] isLoading in
            self?.unitsBuilder.isLoading.value = isLoading
        }
        viewModel.words.bind(self) { [weak self] words in
            self?.unitsBuilder.words.value = words
        }
        viewModel.isMoreLoading.bind(self) { [weak self] isMoreLoading in
            self?.unitsBuilder.isMoreLoading.value = isMoreLoading
        }
        viewModel.allowLoadMore.bind(self) { [weak self] allowLoadMore in
            self?.unitsBuilder.allowLoadMore.value = allowLoadMore
        }
        viewModel.error.bind(self) { [weak self] error in
            self?.unitsBuilder.error.value = error
        }
    }
    
    private func configureUI() {
        navigationItem.title = "Поиск"
        searchView.becomeFirstResponder()
    }
    
    private func configureTableView() {
        tableView.tableHeaderView = searchView
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
        searchView.delegate = self
        searchView.placeholder = "Начни вводить текст"
        searchView.sizeToFit()
        
        tableViewModel.bindTableView(tableView)
    }
    
    private func configureObservers() {
        unitsBuilder.cellModels.bindAndFire(self) { [weak self] models in
            self?.tableViewModel.setSections([models])
        }
    }
    
    private func configureActions() {
        unitsBuilder.loadMoreActon = { [weak self] in
            self?.viewModel.loadMore()
        }
        unitsBuilder.errorAction = { [weak self] in
            self?.viewModel.reload()
        }
        unitsBuilder.didSelectWord = flowWordDetails
    }

}

extension SearchWordsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.performSearchDelayed(searchText)
        unitsBuilder.isSearch.value = !searchText.isEmpty
    }
}
