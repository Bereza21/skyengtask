//
//  SearchWordsUnitsBuilder.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

final class SearchWordsUnitsBuilder {
    
    let cellModels: Observable<[TableCellBinderProtocol]> = .init([])
    
    let isLoading: Observable<Bool> = .init(false)
    
    let isSearch: Observable<Bool> = .init(false)
    
    let words: Observable<[Word]> = .init([])
    
    let isMoreLoading: Observable<Bool> = .init(false)
    
    let allowLoadMore: Observable<Bool> = .init(true)
    
    let error: Observable<Error?> = .init(nil)
    
    var errorAction: EmptyBlock?
    
    var loadMoreActon: EmptyBlock?
    
    var didSelectWord: Block<Word>?
    
    init() {
        isSearch.bind(self) { [weak self] _ in
            self?.buildUnits()
        }
        words.bind(self) { [weak self] _ in
            self?.buildUnits()
        }
        error.bind(self) { [weak self] _ in
            self?.buildUnits()
        }
    }
    
    func buildUnits() {
        var cellUnits: [TableCellBinderProtocol] = []
        guard isSearch.value else {
            cellModels.value = cellUnits
            return
        }
        
        if isLoading.value {
            cellUnits.append(getLoadingCellModel())
        } else if error.value != nil {
            cellUnits.append(getErrorStateCellModel())
        } else if words.value.isEmpty {
            cellUnits.append(getEmptyStateCellModel())
        } else {
            cellUnits.append(contentsOf: getContentCellModels())
        }
        cellModels.value = cellUnits
    }
    
    private func mapWordIntoCellModel(_ index: Int, _ word: Word) -> TableCellBinderProtocol {
        let model = SearchWordsWordTableViewCellModel(
            text: word.text ?? "",
            imageUrl: word.meanings.first?.previewUrl,
            willDisplay: { [weak self] in
                guard let self = self else { return }
                if !self.isMoreLoading.value && self.allowLoadMore.value && (self.words.value.count - index <= 8) {
                    self.loadMoreActon?()
                }
            },
            didSelect: { [weak self] in
                self?.didSelectWord?(word)
            }
        )
        return TableCellBinder<SearchWordsWordTableViewCell>.create(model)
    }
    
    
    private func getLoadingCellModel() -> TableCellBinderProtocol {
        let model = LoadingTableViewCellModel()
        return TableCellBinder<LoadingTableViewCell>.create(model)
    }
    
    private func getEmptyStateCellModel() -> TableCellBinderProtocol {
        let model = EmptyTableViewCellModel(
            message: "Ничего не найдено",
            actionButtonHidden: true,
            action: nil
        )
        return TableCellBinder<EmptyTableViewCell>.create(model)
    }
    
    private func getErrorStateCellModel() -> TableCellBinderProtocol {
        let model = EmptyTableViewCellModel(
            message: "Что-то пошло не так...",
            actionButtonHidden: false,
            action: errorAction
        )
        return TableCellBinder<EmptyTableViewCell>.create(model)
    }
    
    private func getContentCellModels() -> [TableCellBinderProtocol] {
        var models: [TableCellBinderProtocol] = words.value.enumerated().map(mapWordIntoCellModel)
        if allowLoadMore.value {
            models.append(getLoadingCellModel())
        }
        return models
    }
}
