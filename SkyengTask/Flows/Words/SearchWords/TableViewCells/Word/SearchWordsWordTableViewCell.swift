//
//  SearchWordsWordTableViewCell.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit

class SearchWordsWordTableViewCell: UITableViewCell, CellProtocol {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setModel(_ model: SearchWordsWordTableViewCellModel) {
        self.textLabel?.text = model.text
    }

}
