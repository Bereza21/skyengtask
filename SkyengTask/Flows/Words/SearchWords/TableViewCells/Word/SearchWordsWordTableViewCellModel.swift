//
//  SearchWordsWordTableViewCellModel.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit

struct SearchWordsWordTableViewCellModel: CellViewModelProtocol, CellViewModelSelectable, CellViewModelDisplayable {
    
    let text: String
    
    let imageUrl: String?
    
    var willDisplay: (() -> Void)?
    
    var didSelect: (() -> Void)?
    
    static var cellType: UIView.Type {
        return SearchWordsWordTableViewCell.self
    }
    
    static var isHasNib: Bool {
        return false
    }
}

extension SearchWordsWordTableViewCellModel: CellViewModelHeightable {
    
    func heightForTableView(_ tableView: UITableView) -> CGFloat {
        return 44.0
    }
}
