//
//  LoadingTableViewCell.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit

class LoadingTableViewCell: UITableViewCell, CellProtocol {

    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    func setModel(_ model: LoadingTableViewCellModel) {
        activityIndicator.startAnimating()
    }
}
