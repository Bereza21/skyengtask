//
//  LoadingTableViewCellModel.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit

struct LoadingTableViewCellModel: CellViewModelProtocol {
    
    static var cellType: UIView.Type {
        return LoadingTableViewCell.self
    }
    
    static var isHasNib: Bool {
        return true
    }
    
}

extension LoadingTableViewCellModel: CellViewModelHeightable {
    
    func heightForTableView(_ tableView: UITableView) -> CGFloat {
        return 44.0
    }
}
