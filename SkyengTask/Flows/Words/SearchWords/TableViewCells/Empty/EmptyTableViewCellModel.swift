//
//  EmptyTableViewCellModel.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit

struct EmptyTableViewCellModel: CellViewModelProtocol {
    
    let message: String
    
    let actionButtonHidden: Bool
    
    let action: EmptyBlock?
    
    static var cellType: UIView.Type {
        return EmptyTableViewCell.self
    }
    
    static var isHasNib: Bool {
        return true
    }
}

extension EmptyTableViewCellModel: CellViewModelHeightable {
    
    func heightForTableView(_ tableView: UITableView) -> CGFloat {
        return tableView.safeAreaRect.height * 0.7
    }
}
