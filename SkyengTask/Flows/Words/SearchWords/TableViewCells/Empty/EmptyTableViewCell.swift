//
//  EmptyTableViewCell.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit

class EmptyTableViewCell: UITableViewCell, CellProtocol {

    @IBOutlet private weak var messageLbel: UILabel!
    
    @IBOutlet private weak var actionButton: UIButton!
    
    var didTapActon: EmptyBlock?
    
    @IBAction func actionButtonTapped(_ sender: Any) {
        didTapActon?()
    }
    
    func setModel(_ model: EmptyTableViewCellModel) {
        messageLbel.text = model.message
        actionButton.isHidden = model.actionButtonHidden
        didTapActon = model.action
    }
}
