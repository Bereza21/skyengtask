//
//  SearchWordsViewModel.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

final class SearchWordsViewModel {
    
    let words: Observable<[Word]> = .init([])
        
    let isLoading: Observable<Bool> = .init(false)
    
    let isMoreLoading: Observable<Bool> = .init(false)
    
    let allowLoadMore: Observable<Bool> = .init(true)
    
    let error: Observable<Error?> = .init(nil)
    
    private var page: Int = 1
    
    private let pageSize: Int = 30
    
    private var previousSearchText: String?
    
    private var searchTimer: Timer?
    
    func performSearchDelayed(_ text: String) {
        guard text != previousSearchText else {
            return
        }
        guard !text.isEmpty else {
            previousSearchText = ""
            isLoading.value = false
            words.value = []
            return
        }
        
        searchTimer?.invalidate()
        isLoading.value = true
        
        searchTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
            self.searchWord(text: text)
        }
        previousSearchText = text
    }
    
    func reload() {
        searchWord(text: previousSearchText ?? "")
    }
    
    func loadMore() {
        isMoreLoading.value = true
        WordsRequestManager.shared.searchWords(text: previousSearchText ?? "", page: page, pageSize: pageSize) { [weak self] result in
            guard let self = self else { return }
            self.isMoreLoading.value = false
            switch result {
            case .success(let value):
                self.allowLoadMore.value = !(value.words.count < self.pageSize)
                self.words.value = self.words.value + value.words
                self.error.value = nil
                self.page += 1
            case .failure(let error):
                self.error.value = error
            }
        }
    }
    
    private func searchWord(text: String) {
        page = 1
        WordsRequestManager.shared.searchWords(text: text, page: page, pageSize: pageSize) { [weak self] result in
            guard let self = self else { return }
            self.isLoading.value = false
            switch result {
            case .success(let value):
                self.allowLoadMore.value = !(value.words.count < self.pageSize)
                self.words.value = value.words
                self.error.value = nil
                self.page += 1
            case .failure(let error):
                self.error.value = error
            }
        }
    }
}
