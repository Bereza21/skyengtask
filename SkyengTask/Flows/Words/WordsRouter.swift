//
//  WordsRouter.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

final class WordsRouter {
    let navigationController: UINavigationController = .init()
    
    func start() {
        let controller = SearchWordsViewController(nibName: String(describing: SearchWordsViewController.self), bundle: nil)
        let vm = SearchWordsViewModel()
        controller.bindViewModel(vm)
        controller.flowWordDetails = { [weak self] word in
            self?.startWordDetails(word: word)
        }
        navigationController.setViewControllers([controller], animated: false)
    }
    
    func startWordDetails(word: Word) {
        let controller = WordDetailsViewController(nibName: String(describing: WordDetailsViewController.self), bundle: nil)
        let vm = WordDetailsViewModel(word: word)
        controller.bindViewModel(vm)
        controller.flowBack = { [weak navigationController] in
            navigationController?.popViewController(animated: true)
        }
        navigationController.pushViewController(controller, animated: true)
    }
}
