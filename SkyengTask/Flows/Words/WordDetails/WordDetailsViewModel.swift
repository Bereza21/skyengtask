//
//  WordDetailsViewModel.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

final class WordDetailsViewModel {
    
    let word: Word
    
    init(word: Word) {
        self.word = word
    }
}
