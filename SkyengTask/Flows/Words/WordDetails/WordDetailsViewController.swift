//
//  WordDetailsViewController.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit
import Kingfisher

class WordDetailsViewController: ViewController<WordDetailsViewModel> {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var hideNavigationBar: Bool {
        return true
    }

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet private var headerView: UIView!
    
    @IBOutlet private var meaningImageView: UIImageView!
    
    @IBOutlet private var meaningImageViewHeightConstraint: NSLayoutConstraint!
    
    private let unitsBuilder: WordDetailsUnitsBuilder = .init()
    
    private let tableViewModel: BasicTableManager = .init()
    
    var flowBack: EmptyBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTableView()
        configureObservables()
        unitsBuilder.buildUnits(word: viewModel.word)
    }
    
    private func configureTableView() {
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        if let urlString = viewModel.word.meanings.first?.imageUrl,
           let url = URL(string: "https:\(urlString)") {
            meaningImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imPlaceholder"))
        } else {
            meaningImageView.image = #imageLiteral(resourceName: "imPlaceholder")
        }
        tableViewModel.bindTableView(tableView, tableViewDelegate: self)
    }
    
    private func configureObservables() {
        unitsBuilder.cellModels.bind(self) { [weak self] models in
            self?.tableViewModel.setSections([models])
        }
    }

    @IBAction private func back() {
        flowBack?()
    }
}

extension WordDetailsViewController: UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let header = tableView.tableHeaderView else {
            return
        }
        meaningImageViewHeightConstraint.constant = max(header.bounds.height, header.bounds.height - scrollView.contentOffset.y)
    }
}
