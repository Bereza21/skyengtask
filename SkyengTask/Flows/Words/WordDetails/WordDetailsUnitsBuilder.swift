//
//  WordDetailsUnitsBuilder.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

final class WordDetailsUnitsBuilder {
    
    let cellModels: Observable<[TableCellBinderProtocol]> = .init([])
    
    func buildUnits(word: Word) {
        cellModels.value = [getTranslationCellModel(word: word)]
    }
    
    private func getTranslationCellModel(word: Word) -> TableCellBinderProtocol {
        let model = WordDetailsTranslationTableViewCellModel(
            wordValue: word.text,
            translation: word.meanings.first?.translation.text
        )
        return TableCellBinder<WordDetailsTranslationTableViewCell>.create(model)
    }
}
