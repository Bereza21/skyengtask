//
//  WordDetailsTranslationTableViewCellModel.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit

struct WordDetailsTranslationTableViewCellModel: CellViewModelProtocol {
    
    let wordValue: String?
    
    let translation: String?
    
    static var cellType: UIView.Type {
        return WordDetailsTranslationTableViewCell.self
    }
    
    static var isHasNib: Bool {
        return true
    }
    
}
