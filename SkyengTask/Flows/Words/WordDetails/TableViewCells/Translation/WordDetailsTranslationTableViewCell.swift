//
//  WordDetailsTranslationTableViewCell.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import UIKit

class WordDetailsTranslationTableViewCell: UITableViewCell, CellProtocol {

    @IBOutlet private weak var wordLabel: UILabel!
    
    @IBOutlet private weak var translationLabel: UILabel!
    
    func setModel(_ model: WordDetailsTranslationTableViewCellModel) {
        wordLabel.text = model.wordValue
        translationLabel.text = model.translation
    }
    
}
