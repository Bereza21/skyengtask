//
//  AppDelegate.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    private var router: WordsRouter?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let router = WordsRouter()
        self.window?.rootViewController = router.navigationController
        self.window?.makeKeyAndVisible()
        router.start()
        
        self.router = router
        
        return true
    }

}

