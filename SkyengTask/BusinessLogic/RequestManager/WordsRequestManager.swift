//
//  WordsRequestManager.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

final class WordsRequestManager {
    
    static let shared: WordsRequestManager = .init()
    
    func searchWords(text: String, page: Int, pageSize: Int, complete: @escaping Block<Result<SearchWordsResponse, Error>>) {
        let params: [String: String] = [
            "search": text,
            "page": String(page),
            "pageSize": String(pageSize)
        ]
        let request = BasicRequest<EmptyBody, SearchWordsResponse>("https://dictionary.skyeng.ru/api/public/v1/words/search")
        request.queryParams = params
        request.perform(complete)
    }
}
