//
//  SearchWordsResponse.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

struct SearchWordsResponse: Codable {
    let words: [Word]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.words = try container.decode([Word].self)
    }
}

struct Word: Codable {
    let id: Int
    let text: String?
    let meanings: [Meaning]
}

struct Meaning: Codable {
    let id: Int
    let previewUrl: String?
    let imageUrl: String?
    let translation: Translation
}

struct Translation: Codable {
    let text: String
}
