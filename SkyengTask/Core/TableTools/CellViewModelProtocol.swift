//
//  CellViewModelProtocol.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

protocol CellViewModelProtocol {
    static var reuseIdentifier: String { get }
    static var cellType: UIView.Type { get }
    static var bundle: Bundle { get }
    static var isHasNib: Bool { get }
}


extension CellViewModelProtocol {
    static var reuseIdentifier: String {
        return "\(self.cellType).\(self)"
    }

    static var bundle: Bundle {
        return Bundle(for: self.cellType)
    }
}
