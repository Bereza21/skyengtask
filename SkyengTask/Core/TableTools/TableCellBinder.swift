//
//  TableCellBinder.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

protocol TableCellBinderProtocol {
    var reuseIdentifier: String { get }

    func bindToCell(_ cell: UITableViewCell)
    func cellModel() -> CellViewModelProtocol
}


final class TableCellBinder<T: CellProtocol & UITableViewCell>: BasicCellBinder<T>, TableCellBinderProtocol {
    func bindToCell(_ _cell: UITableViewCell) {
        guard let cell: T = _cell as? T else {
            assertionFailure("Invalid cell type! Expected '\(T.self)', actual '\(_cell.self)'")
            return
        }

        cell.setModel(self.model)
    }
    func cellModel() -> CellViewModelProtocol {
        return self.model
    }

    static func create(_ model: T.Model) -> TableCellBinder<T> {
        return TableCellBinder<T>(model: model,
                                  reuseIdentifier: type(of: model).reuseIdentifier,
                                  cellType: type(of: model).cellType,
                                  bundle: type(of: model).bundle)
    }
}
