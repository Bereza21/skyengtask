//
//  BasicCellBinder.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

protocol CellProtocol {
    associatedtype Model: CellViewModelProtocol

    func setModel(_ model: Model)
}

class BasicCellBinder<T: CellProtocol> {
    let model: T.Model
    let reuseIdentifier: String
    let cellType: UIView.Type
    let bundle: Bundle

    init(model: T.Model, reuseIdentifier: String, cellType: UIView.Type, bundle: Bundle) {
        self.model = model
        self.reuseIdentifier = reuseIdentifier
        self.cellType = cellType
        self.bundle = bundle
    }
}
