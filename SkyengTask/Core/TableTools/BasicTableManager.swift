//
//  BasicTableManager.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

class BasicTableManager: NSObject, UITableViewDataSource, UITableViewDelegate {
    private weak var tableView: UITableView?
    private var sections: [[TableCellBinderProtocol]] = []
    
    weak var tableViewDelegate: UITableViewDelegate?

    func bindTableView(_ tableView: UITableView, tableViewDelegate: UITableViewDelegate? = nil) {
        self.sections.removeAll()

        self.tableView = tableView
        self.tableViewDelegate = tableViewDelegate
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
    }

    func setSections(_ sections: [[TableCellBinderProtocol]]) {
        self.sections = sections

        for section: [TableCellBinderProtocol] in sections {
            for item: TableCellBinderProtocol in section {
                self.registerCellModel(item)
            }
        }

        self.tableView?.reloadData()
    }

    private func registerCellModel(_ item: TableCellBinderProtocol) {
        let model: CellViewModelProtocol = item.cellModel()
        let reuseIdentifier: String = type(of: model).reuseIdentifier

        if type(of: model).isHasNib {
            let bundle: Bundle = type(of: model).bundle
            let nib: UINib = UINib(nibName: "\(type(of: model).cellType)", bundle: bundle)
            self.tableView?.register(nib, forCellReuseIdentifier: reuseIdentifier)
        } else {
            self.tableView?.register(type(of: model).cellType, forCellReuseIdentifier: reuseIdentifier)
        }

        if var model = model as? CellViewModelHeightChangeable {
            model.didChangeHeight = { [weak self] in
                self?.tableView?.beginUpdates()
                self?.tableView?.endUpdates()
            }
        }
    }

    private func dequeueReusableCell(unit: TableCellBinderProtocol, indexPath: IndexPath) -> UITableViewCell {
        guard let tableView: UITableView = self.tableView else {
            assertionFailure("It seems your UITableView is dead")
            return UITableViewCell()
        }

        let indetifier: String = unit.reuseIdentifier
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: indetifier, for: indexPath)

        unit.bindToCell(cell)

        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.dequeueReusableCell(unit: self.sections[indexPath.section][indexPath.row], indexPath: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let items = self.sections[indexPath.section][indexPath.row].cellModel() as? CellViewModelHeightable else {
            return UITableView.automaticDimension
        }
        return items.heightForTableView(tableView)
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let items = self.sections[indexPath.section][indexPath.row].cellModel() as? CellViewModelHeightable else {
            return UITableView.automaticDimension
        }
        return items.heightForTableView(tableView)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        (self.sections[indexPath.section][indexPath.row].cellModel() as? CellViewModelSelectable)?.didSelect?()

        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (self.sections[indexPath.section][indexPath.row].cellModel() as? CellViewModelDisplayable)?.willDisplay?()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tableViewDelegate?.scrollViewDidScroll?(scrollView)
    }
}
