//
//  Actions.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import UIKit

protocol CellViewModelSelectable {
    var didSelect: (() -> Void)? { get set }
}

protocol CellViewModelHeightable {
    func heightForTableView(_ tableView: UITableView) -> CGFloat
}

protocol CellViewModelDisplayable {
    var willDisplay: (() -> Void)? { get set }
}

protocol CellViewModelHeightChangeable {
    var didChangeHeight: (() -> Void)? { get set }
}
