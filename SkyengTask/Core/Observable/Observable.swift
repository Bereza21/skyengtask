//
//  Observable.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

class WeakObject {
    weak var object: AnyObject?

    init(object: AnyObject?) {
        self.object = object
    }
}


class Observable<T> {
    var value: T {
        didSet {
            self.weakObservers = self.weakObservers.filter{observer in observer.owner.object != nil}

            for observer in self.weakObservers {
                observer.action(self.value)
            }
        }
    }

    private var weakObservers: [(owner: WeakObject, action: ((T) -> Void))] = []

    init(_ value: T) {
        self.value = value
    }

    func bind(_ owner: AnyObject, action: @escaping ((T) -> Void)) {
        self.weakObservers.append((owner: WeakObject(object: owner), action: action))
    }
    
    func bindAndFire(_ owner: AnyObject, action: @escaping ((T) -> Void)) {
        self.weakObservers.append((owner: WeakObject(object: owner), action: action))
        action(self.value)
    }

    func removeAllObservers() {
        self.weakObservers = []
    }

    func notifyAllObservers() {
        for observer in self.weakObservers {
            observer.action(self.value)
        }
    }
}

