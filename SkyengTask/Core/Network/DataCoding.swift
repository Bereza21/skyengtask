//
//  DataCoding.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

class BasicResponseDecoder<T: Decodable> {
    private let decoder = JSONDecoder()

    init() {
        self.decoder.dateDecodingStrategy = .iso8601
    }

    func decode(data: Data) throws -> T {
        return try self.decoder.decode(T.self, from: data)
    }
}

class BasicBodyEncoder<T: Encodable> {
    private let encoder = JSONEncoder()

    init() {
        self.encoder.dateEncodingStrategy = .iso8601
    }

    func encode(object: T) throws -> Data {
        return try encoder.encode(object)
    }
}
