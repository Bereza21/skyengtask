//
//  StatusCodeMapping.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

protocol StatusCodeMapperProtocol {
    func mapStatusCode(_ code: Int) -> Error?
}

class BasicStatusCodeMapper: StatusCodeMapperProtocol {
    private let validCodesRange: Range<Int> = 200..<300

    func mapStatusCode(_ code: Int) -> Error? {
        if self.validCodesRange.contains(code) {
            return nil
        } else {
            switch code {
            case 401:
                return BasicRequestInternalError.unauthorized
            case 500:
                return BasicRequestInternalError.internalServerError
            default:
                return BasicRequestInternalError.unknownError
            }
        }
    }
}
