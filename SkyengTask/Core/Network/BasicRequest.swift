//
//  BasicRequest.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

typealias Block<T> = ((T) -> Void)
typealias EmptyBlock = () -> Void

enum RequestMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case delete = "DELETE"
}

enum BasicRequestInternalError {
    static let invalidURL = AppError("Failed to create request URL")
    static let internalServerError = AppError("Internal server error")
    static let unauthorized = AppError("Unauthorized")
    static let unknownError = AppError("Unknown error")
    static let noDataReceived = AppError("No data received from the server")
}

struct EmptyResponse: Decodable { }

struct NullResponse: Decodable { }

struct EmptyBody: Encodable { }

class BasicRequest<Body: Encodable, Response: Decodable> {
    let urlStr: String
    var method: RequestMethod = .get
    var responseDecoder: BasicResponseDecoder<Response> = BasicResponseDecoder<Response>()
    var statusCodeMapper: StatusCodeMapperProtocol? = BasicStatusCodeMapper()
    var session = URLSession(configuration: .default)
    var headers: [String: String?]?
    var queryParams: [String: String?]?
    var body: Body?
    var bodyEncoder = BasicBodyEncoder<Body>()
    var timeout: TimeInterval = 60.0
    private var completion: Block<Result<Response, Error>>?

    init(_ urlStr: String, method: RequestMethod = .get, body: Body? = nil, headers: [String: String?]? = nil) {
        self.urlStr = urlStr
        self.method = method
        self.body = body
        self.headers = headers
    }

    @discardableResult
    func perform(_ completion: Block<Result<Response, Error>>?) -> URLSessionTask? {
        self.completion = completion
        let request: URLRequest

        do {
            request = try self.createRequest()
        } catch {
            completion?(.failure(error))
            return nil
        }

        let task: URLSessionTask = self.createTask(request: request, completion: completion)
        task.resume()

        return task
    }

    func createTask(request: URLRequest, completion: Block<Result<Response, Error>>?) -> URLSessionTask {
        let task: URLSessionDataTask = self.session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            self.processResponse(data: data, response: response, error: error, completion: completion)
        }

        return task
    }

    func processResponse(data: Data?, response: URLResponse?, error: Error?, completion: Block<Result<Response, Error>>?) {

        if let error = error {
            self.doCompletion(.failure(error))
            return
        }

        if let httpResponse = response as? HTTPURLResponse {
            if let error = self.errorFromStatusCode(httpResponse.statusCode) {
                self.doCompletion(.failure(error))
                return
            }
        }

        do {
            if type(of: Response.self) == type(of: NullResponse.self) {
                self.doCompletion(.success(NullResponse() as! Response))
            } else {
                if let data = data {
                    let object: Response = try self.responseDecoder.decode(data: data)
                    self.doCompletion(.success(object))
                } else {
                    self.doCompletion(.failure(BasicRequestInternalError.noDataReceived))
                }
            }
        } catch {
            self.doCompletion(.failure(error))
        }
    }

    private func errorFromStatusCode(_ code: Int) -> Error? {
        return self.statusCodeMapper?.mapStatusCode(code)
    }

    private func createRequest() throws -> URLRequest {
        let url = try self.createURL()

        var request = URLRequest(url: url)
        request.timeoutInterval = self.timeout
        request.httpMethod = self.method.rawValue

        for (k, v) in self.headers ?? [:] {
            request.setValue(v, forHTTPHeaderField: k)
        }

        if let body = self.body {
            request.httpBody = try self.bodyEncoder.encode(object: body)

            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }

        return request
    }

    private func createURL() throws -> URL {
        guard var components = URLComponents(string: self.urlStr) else {
            throw BasicRequestInternalError.invalidURL
        }

        if let queryParams = self.queryParams {
            var items = [URLQueryItem]()

            for (k, v) in queryParams {
                items.append(URLQueryItem(name: k, value: v))
            }

            components.queryItems = items
        }

        if let url = components.url {
            return url
        }

        throw BasicRequestInternalError.invalidURL
    }

    private func doCompletion(_ result: Result<Response, Error>) {
        DispatchQueue.main.async {
            self.completion?(result)
        }
    }
}
