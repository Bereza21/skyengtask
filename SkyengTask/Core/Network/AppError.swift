//
//  AppError.swift
//  SkyengTask
//
//  Created by Vadim Khokhryakov on 29.04.2021.
//

import Foundation

class AppError: Error, LocalizedError {
    private let message: String

    var errorDescription: String? {
        return self.message
    }

    var failureReason: String? {
        return self.message
    }

    var recoverySuggestion: String? {
        return nil
    }

    var helpAnchor: String? {
        return nil
    }

    init(_ localizedDescription: String) {
        self.message = localizedDescription
    }
}
