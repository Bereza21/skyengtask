//
//  SkyengTaskTests.swift
//  SkyengTaskTests
//
//  Created by Vadim Khokhryakov on 28.04.2021.
//

import XCTest
@testable import SkyengTask

class SkyengTaskTests: XCTestCase {
    
    var sut: SearchWordsViewModel!

    override func setUpWithError() throws {
        sut = SearchWordsViewModel()
    }

    override func tearDownWithError() throws {
        sut = nil
    }

    func testFetchLoading() throws {
        XCTAssertFalse(sut.isLoading.value)
        sut.performSearchDelayed("box")
        XCTAssertTrue(sut.isLoading.value)
        
        let expect = XCTestExpectation(description: "Fetch loading status changed")
        sut.isLoading.bind(self) { isLoading in
            XCTAssertFalse(isLoading)
            expect.fulfill()
        }
        
        wait(for: [expect], timeout: 5.0)
    }

}
